<?php
use App\Http\Controllers\ProductController;
$total=0;
if (Session::has('user')){
    $total= ProductController::cartitem();
}

?>
<div class="navbar">
    <div class="logo">
        <img src="{{asset("images/logo.jpg")}}" width="125px">
    </div>
    <nav>
        <ul id="MenuItems">
            <li><a href="/">Home</a></li>
            <li><a href="/produk">Product</a></li>
            <li><a href="/myorders">Orders</a></li>
            <li><a href="/cartlist">cart({{ $total }})</a></li>
            @if(Session::has('user'))
            <ul>
                <li>{{ Session::get('user')['name'] }}</li>
                <li><a href="/logout">Logout</a></li>
            </ul>
            @else
              <li><a href="/login">Login</a></li>
              <li><a href="/register">Register</a></li>
            @endif
        </ul>
    </nav>
</div>