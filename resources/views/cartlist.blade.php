@extends('master')
@section('content')
<div class="container">
    <div class="col-sm-10">
        <div>
            <h4>Result For Products</h4>
            <a class="btn btn-success" href="ordernow">Order Now</a> <br> <br>
            @foreach ($products as $item)
                <div class="row cart-list-devider">
                    <div class="col-sm-3">
                        <a href="detail/{{ $item->id }}">
                    <img class="result-img" src="{{ $item->galery }}">  
                    </a>
                    </div>
                    <div class="col-sm-3">
                    <div>
                        <h2>{{ $item->name }}</h2>
                        <h5>{{ $item->description }}</h5>
                        <h6><b>Rp {{ $item->price }}</b></h6>
                    </div>
                    </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="/removecart/{{ $item->cart_id }}" class="btn btn-warning">Remove from cart</a>
                    </div>
                </div>
            @endforeach
        </div>
        <a class="btn btn-success" href="ordernow">Order Now</a> <br> <br>
    </div>
</div>
@endsection