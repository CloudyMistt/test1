<?php
use App\Http\Controllers\ProductController;
$total= ProductController::cartitem();
?>
<div class="navbar">
    <div class="logo">
        <img src="{{asset("images/logo.jpg")}}" width="125px">
    </div>
    <nav>
        <ul id="MenuItems">
            <li><a href="/">Home</a></li>
            <li><a href="/produk">Product</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/contact">Contact</a></li>
            <li><a href="#">cart({{ $total }})</a></li>
        </ul>
    </nav>
</div>