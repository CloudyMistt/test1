@extends('master')
@section('content')
<div class="container">
        <div class="col-sm-6">
            <img class="detail-img" src="{{$product['galery']}}">
        </div>
        <div class="col-sm-6">
            <a href="/">Go back</a>
            <h3>{{ $product['brand'] }}</h3>
            <h2>{{ $product['name'] }}</h2>
            <h3><b>Harga :</b> {{ $product['price'] }}</h3>
            <h4><b>Deskripsi :</b> {{ $product['description'] }}</h4>
            <br><br>
            <form action="/add_to_cart" method="POST">
                @csrf
                <input type="hidden" name="product_id" value="{{ $product['id'] }}">
                <button class="btn btn-primary">Add to CART</button>
            </form>
            <br><br>
            <button class="btn btn-success">Buy Now</button>
            <br>
        </div>
</div>
@endsection