
    <div class="small-container">
        <h2 class="title">Produk Terbaru</h2><br><br>
        <div class="row">
        @foreach ($products as $item)
            <div class="col-sm-4">
                <div class="item  {{$item->id==1?'active':''}}>">
                    <a href="detail/{{$item->id}}">
                        <img class="img-p" src="{{$item->galery}}" >
                        <h3> {{$item->name}}</h3>
                        <h3> {{$item->brand}}</h3>
                        <h3> {{$item->price}}</h3>
                        <p>{{$item->description}}</p>
                    </a>
                </div>
            </div> 
        @endforeach
        </div>
    </div>
