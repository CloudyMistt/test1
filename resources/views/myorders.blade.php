@extends('master')
@section('content')
<div class="container">
    <div class="col-sm-10">
        <div>
            <h4>My Orders</h4>
            @foreach ($orders as $item)
                <div class="row cart-list-devider">
                    <div class="col-sm-3">
                        <a href="detail/{{ $item->id }}">
                    <img class="result-img" src="{{ $item->galery }}">  
                    </a>
                    </div>
                    <div class="col-sm-3">
                    <div>
                        <h2>Name : {{ $item->name }}</h2>
                        <h5>Status Pengiriman : {{ $item->status }}</h5>
                        <h5>Address : {{ $item->address }}</h5>
                        <h5>Status Pembayaran : {{ $item->payment_status }}</h5>
                        <h5>Metode Pembayaran : {{ $item->payment_method}}</h5>
                        <h6><b>Price : Rp {{ $item->price }}</b></h6>
                    </div>
                    </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection