@extends('master')
@section('content')
<div class="container">
    <div class="col-sm-10">
        <table class="table">
            <tbody>
                <tr>
                    <td>Amount</td>
                    <td>Rp {{ $total }}</td>
                </tr>
                <tr>
                    <td>Tax</td>
                    <td>Rp 0</td>
                </tr>
                <tr>
                    <td>Delivery</td>
                    <td>Rp 20000</td>
                </tr>
                <tr>
                    <td>Total Amount</td>
                    <td>Rp {{ $total+20000 }}</td>
                </tr>
            </tbody>
        </table>
        <div>
            <form action="/orderplace" method="POST">
            @csrf
                <div class="form-group">
                    <textarea name="address" placeholder="Masukan Alamat Anda" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="">Metode Pembayaran</label><br>
                    <input type="radio" value="cash" name="payment">  <span>Pembayaran Online</span> <br> <br>
                    <input type="radio" value="cash" name="payment">  <span>Pembayaran ditempat</span> <br> <br>
                </div>
                <button type="submit" class="btn btn-primary">Order Now</button>
            </form>
        </div>
    </div>
</div>
@endsection