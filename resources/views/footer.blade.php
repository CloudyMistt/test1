<div class="footer">
    <div class="container">
        <div class="row">
            <div class="footer-col-1">
                <h3>Download Our App</h3>
                <p>Download App for Android and ios mobile phone.</p>
                <div class="app-logo">
                    <img src="{{asset("images/play-store.png")}}">
                    <img src="{{asset("images/app-store.png")}}">
                </div>
            </div>
            <div class="footer-col-2">
                <img src="{{asset("images/logo.jpg")}}">
                <p>tujuan kami adalah untuk membuat kesenangan dan membuat sesorang dapat memiliki smartphone idamannya!</p>
            </div>
            <div class="footer-col-3">
                <h3>Link yang mungkin berguna</h3>
                <ul>
                    <li>Kupon</li>
                    <li>Blog Post</li>
                    <li>Kebijakan pengembalian</li>
                </ul>
            </div>
            <div class="footer-col-4">
                <h3>Follow us</h3>
                <ul>
                    <li>Facebook</li>
                    <li>Instagram</li>
                    <li>Twitter</li>
                    <li>Youtube</li>
                </ul>
            </div>
        </div>
        <hr>
        <p class="Copyright">Copyright 2021 PhoneStore</p>
    </div>
</div>