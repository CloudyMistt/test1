<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Online Shop</title>
    <link rel="stylesheet" href="style.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
{{View::make('header')}}
<!--------- login page ------->
 
<div class="account-page">
    <div class="container">
        <div class="row">
            <div class="col-2">
                <img src="{{asset("images/background.jpg")}}" width="600px" height="400px">
            </div>
        <form action="login" method="POST">
            @csrf
            <h3><b>Masuk ke website</b></h3>
            <p><b>Belum punya akun?</b><a href="register"><h4>Daftar di sini</h4></a></p>
       
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control" type="text" name="email" placeholder="Email" />
            </div>


            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" placeholder="Password" />
            </div>

            <button type="submit" class="btn btn-success btn-block">Login</button>

        </form>
        </div>
    </div>
</div>
{{View::make('footer')}}
</html>