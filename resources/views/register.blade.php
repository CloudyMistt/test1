@extends('master')
@section('content')
<div class="account-page">
    <div class="container">
        <div class="row">
            <div class="col-2">
                <img src="{{asset("images/background.jpg")}}" width="600px" height="400px">
            </div>
        <form action="register" method="POST">
            @csrf
            <h3><b>Daftar ke website</b></h3>
            <p><b>Sudah punya akun?</b><a href="login"><h4>Login di sini</h4></a></p>
       
            <div class="form-group">
                <label for="nama">Nama</label>
                <input class="form-control" type="text" name="name" placeholder="Nama" />
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control" type="text" name="email" placeholder="Email" />
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" placeholder="Password" />
            </div>

            <button type="submit" class="btn btn-success btn-block">Register</button>

        </form>
        </div>
    </div>
</div>
@endsection