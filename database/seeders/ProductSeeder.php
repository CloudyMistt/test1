<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
            [
                'name'=>'Samsung Galaxy A32',
                'brand'=>'Samsung',
                'price'=>'3600000',
                'description'=>'Resolusi Kamera Belakang : 64 + 8 + 5 + 5mp,Resolusi Kamera Depan : 20mp,Ram : 8GB,Penyimpanan : 128GB,Resolusi Video : 4k',
                'galery'=>'https://d2pa5gi5n2e1an.cloudfront.net/global/images/product/mobilephones/Samsung_Galaxy_A32_5G/Samsung_Galaxy_A32_5G_L_1.jpg',
            ],
            [
                'name'=>'Poco F3',
                'brand'=>'Xiaomi',
                'price'=>'5099000',
                'description'=>'Resolusi Kamera Belakang : 48 + 8 + 5mp,Resolusi Kamera Depan : 20mp,Tipe Kamera : Triple Kamera,Resolusi Video : 4k,Penyimpanan : 128GB,Ram : 6GB',
                'galery'=>'http://p.ipricegroup.com/uploaded_13a69c08fbe879d1b98003c8997d8a81.jpg',
            ],
            [
                'name'=>'Iphone 11 Pro Max',
                'brand'=>'Apple',
                'price'=>'17499000',
                'description'=>'Resolusi Kamera Belakang : 12 + 12 + 12mp,Resolusi Kamera Depan : 12mp,Tipe Kamera : Triple Kamera,Resolusi Video : 4k,Ram : 4GB,Penyimpanan : 256GB',
                'galery'=>'https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full//77/MTA-5101141/apple_apple_iphone_11_pro_max_256_gb_-garansi_resmi-_full08_tb18zwf4.jpg',
            ],
            [
                'name'=>'Samsung Galaxy A52',
                'brand'=>'Samsung',
                'price'=>'5150000',
                'description'=>'Resolusi Kamera Belakang : 64 + 12 + 5 + 5mp,Resolusi Kamera Depan : 32mp,Ram : 8GB,Penyimpanan : 128GB,Resolusi Video : 4k',
                'galery'=>'https://d2pa5gi5n2e1an.cloudfront.net/global/images/product/mobilephones/Samsung_Galaxy_A52_5G/Samsung_Galaxy_A52_5G_L_1.jpg',
            ],
            [
                'name'=>'Poco X3 Pro',
                'brand'=>'Xiaomi',
                'price'=>'3549000',
                'description'=>'Resolusi Kamera Belakang : 48 + 8 + 2 + 2mp,Resolusi Kamera Depan : 20mp,Tipe Kamera : Quad Kamera,Resolusi Video : 4k,Penyimpanan : 128GB,Ram : 6GB',
                'galery'=>'https://id-test-11.slatic.net/p/e912b9debf2b6fae0d55af2bbc13b3d1.jpg',
            ],
            [
                'name'=>'Iphone X 256GB',
                'brand'=>'Apple',
                'price'=>'6499000',
                'description'=>'Resolusi Kamera Belakang : 12mp,Resolusi Kamera Depan : 12mp,Tipe Kamera : Dual Kamera,Resolusi Video : Full HD,Ram : 3GB,Penyimpanan : 256GB',
                'galery'=>'http://p.ipricegroup.com/uploaded_8ce086d995acddd8b32410a42696777f.jpg',
            ]
            
        ]);
    }
}
