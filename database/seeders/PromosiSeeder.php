<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PromosiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('promosis')->insert([
            [
                'name'=>'Iphone SE 64GB',
                'brand'=>'Apple',
                'price'=>'7.399.000',
                'description'=>'Chipset : Apple A13 Bionic(7nm+)Neural Engine Generasi ketiga,Ram 3GB,Kamera Depan : singgle kamera 7mp,Kamera Belakang : 12mp f/1.8(wide),PDAF,OIS,Tipe Layar : Retina',
                'galery'=>'https://media.pricebook.co.id/images/product/L/46188_L_1.jpg',
            ]
        ]);
        
    }
}
